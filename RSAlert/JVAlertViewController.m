//
//  JVAlertViewController.m
//  JVAlert
//
//  Created by Сергей on 13.09.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#define ALERT_TEXT_HORIZONTAL_MARGIN 5.0f
#define ALERT_TEXT_VERTICAL_MARGIN 5.0f
#define ALERT_VIEW_WIDTH 310.0f
#define ALERT_VIEW_HEIGHT 300.0f
#define BUTTON_HEIGHT 65.0f
#define CONRNER_RADIUS 10.0f


#import "JVAlertViewController.h"


@implementation JVAlertViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated
{
    [self.view addSubview:[self createDarkBackGroundView]];
    [self.view addSubview: [self createAlertView]];
    
}

- (instancetype)initWithAlertMessage:(NSString *)message
{
    self = [[JVAlertViewController alloc] initWithNibName:nil bundle:nil];
    if (self)
    {
        _alertMessage = message;
        _dismissByBackgoundTap = NO;
        self.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    }
    return self;
}

- (UIView *) createAlertView
{
    CGFloat width = ALERT_VIEW_WIDTH;
    CGFloat height = ALERT_VIEW_HEIGHT;
    CGRect rect = CGRectMake(self.view.center.x - width / 2, self.view.center.y - height / 2, width, height);
    
    UIView * alertView = [[UIView alloc] initWithFrame:rect];
    alertView.backgroundColor = [UIColor whiteColor];
    alertView.alpha = 1.0f;
    alertView.layer.cornerRadius = CONRNER_RADIUS;
    alertView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;
    
    [self addLabelsOnView:alertView];
    [self addButtonsOnView:alertView];
    
    return alertView;
}

- (UIView *) createDarkBackGroundView
{
    UIView * darkView = [[UIView alloc] initWithFrame:self.view.bounds];
    darkView.backgroundColor = [UIColor blackColor];
    darkView.alpha = 0.4f;
    
    darkView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    if (self.dismissByBackgoundTap)
    {
        [darkView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didPressDeclineButton)]];
    }
    
    return darkView;
}

- (void) addLabelsOnView: (UIView*) view
{
    CGRect rect = CGRectMake(view.bounds.origin.x + ALERT_TEXT_VERTICAL_MARGIN,
                             view.bounds.origin.y + ALERT_TEXT_HORIZONTAL_MARGIN,
                             view.bounds.size.width - ALERT_TEXT_HORIZONTAL_MARGIN * 2,
                             view.bounds.size.height - ALERT_TEXT_VERTICAL_MARGIN * 2 - BUTTON_HEIGHT);
    UILabel * alertLabel = [[UILabel alloc] initWithFrame: rect];
    alertLabel.text = [self.alertMessage uppercaseString];
    alertLabel.numberOfLines = 0;
    alertLabel.textAlignment = NSTextAlignmentCenter;
    alertLabel.font = self.generalFont;
    alertLabel.textColor = [UIColor colorWithRed:94/255.0 green:94/255.0 blue:94/255.0 alpha:1.0];
    [view addSubview:alertLabel];
}

- (void) addButtonsOnView: (UIView *) view
{
    
    CGFloat buttonWidth = view.bounds.size.width / 2;
    CGFloat buttonHeight = BUTTON_HEIGHT;
    CGFloat buttonY = CGRectGetMaxY(view.bounds) - buttonHeight;
    
    //ACCEPT BUTTON
    CGRect acceptButtonRect = CGRectMake(self.view.bounds.origin.x, buttonY, buttonWidth, buttonHeight);
    UIButton * acceptButton = [[UIButton alloc] initWithFrame:acceptButtonRect];
    [acceptButton setTitle:self.acceptButtonTitle forState:UIControlStateNormal];
    acceptButton.backgroundColor = [UIColor colorWithRed:239/255.0 green:103/255.0 blue:111/255.0 alpha:1.0];
    acceptButton.titleLabel.font = self.generalFont;
    [acceptButton addTarget:self action:@selector(didPressAcceptButton) forControlEvents:UIControlEventTouchUpInside];
    //round corner
    UIBezierPath *maskPath1 = [UIBezierPath
                              bezierPathWithRoundedRect:acceptButton.bounds
                              byRoundingCorners:(UIRectCornerBottomLeft)
                              cornerRadii:CGSizeMake(CONRNER_RADIUS, CONRNER_RADIUS)
                              ];
    
    CAShapeLayer *maskLayer1 = [CAShapeLayer layer];
    
    maskLayer1.frame = acceptButton.bounds;
    maskLayer1.path = maskPath1.CGPath;
    
    acceptButton.layer.mask = maskLayer1;
    
    //DECLINE BUTTON
    CGRect declineButtonRect = CGRectMake(self.view.bounds.origin.x + buttonWidth, buttonY, buttonWidth, buttonHeight);
    UIButton * declineButton = [[UIButton alloc] initWithFrame:declineButtonRect];
    [declineButton setTitle:self.declineButtonTitle forState:UIControlStateNormal];
    declineButton.backgroundColor = [UIColor colorWithRed:205/255.0 green:205/255.0 blue:205/255.0 alpha:1.0];
    declineButton.titleLabel.font = self.generalFont;
    [declineButton addTarget:self action:@selector(didPressDeclineButton) forControlEvents:UIControlEventTouchUpInside];
    
    //round corner
    UIBezierPath *maskPath2 = [UIBezierPath
                              bezierPathWithRoundedRect:declineButton.bounds
                              byRoundingCorners:(UIRectCornerBottomRight)
                              cornerRadii:CGSizeMake(CONRNER_RADIUS, CONRNER_RADIUS)
                              ];
    
    CAShapeLayer *maskLayer2 = [CAShapeLayer layer];
    
    maskLayer2.frame = declineButton.bounds;
    maskLayer2.path = maskPath2.CGPath;
    
    declineButton.layer.mask = maskLayer2;
    
    [view addSubview:acceptButton];
    [view addSubview:declineButton];
}

#pragma mark - private

- (NSString *)acceptButtonTitle
{
    if (!_acceptButtonTitle)
    {
        _acceptButtonTitle = NSLocalizedString(@"YES", nil);
    }
    
    return _acceptButtonTitle;
}

- (NSString *)declineButtonTitle
{
    if (!_declineButtonTitle)
    {
        _declineButtonTitle = NSLocalizedString(@"NO", nil);
    }
    return _declineButtonTitle;
}

#pragma mark - Actions

- (void) actionAcceptWithTitle: (NSString *) title actionHandler: (ActionHandler) actionHandler
{
    self.acceptButtonTitle = title;
    if (actionHandler)
    {
        self.acceptHandler = actionHandler;
    }
}

- (void) actionDeclineWithTitle: (NSString *) title actionHandler: (ActionHandler) actionHandler
{
    self.declineButtonTitle = title;
    if (actionHandler)
    {
        self.declineHandler = actionHandler;
    }
}

- (void) didPressAcceptButton
{
    if (self.acceptHandler)
    {
        self.acceptHandler();
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) didPressDeclineButton
{
    if (self.declineHandler)
    {
        self.declineHandler();
    }
    [self dismissViewControllerAnimated:YES completion:nil];

}

@end
