//
//  AppDelegate.h
//  RSAlert
//
//  Created by Сергей on 12.09.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

