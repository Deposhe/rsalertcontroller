//
//  JVAlertViewController.h
//  JVAlert
//
//  Created by Сергей on 13.09.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^ActionHandler)(void);

@interface JVAlertViewController : UIViewController

@property (nonatomic, strong) NSString * alertMessage;
@property (nonatomic) BOOL dismissByBackgoundTap; //default NO
@property (nonatomic, strong) UIFont * generalFont;
@property (nonatomic, strong) NSString * acceptButtonTitle;
@property (nonatomic, strong) NSString * declineButtonTitle;

@property (nonatomic, copy) ActionHandler acceptHandler;
@property (nonatomic, copy) ActionHandler declineHandler;

- (instancetype) initWithAlertMessage: (NSString*) message;
- (void) actionAcceptWithTitle: (NSString *) title actionHandler: (ActionHandler) actionHandler;
- (void) actionDeclineWithTitle: (NSString *) title actionHandler: (ActionHandler) actionHandler;

@end
