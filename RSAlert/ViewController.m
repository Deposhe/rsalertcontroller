//
//  ViewController.m
//  RSAlert
//
//  Created by Сергей on 12.09.16.
//  Copyright © 2016 Serge Rylko. All rights reserved.
//

#import "ViewController.h"
#import "JVAlertViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)didPressPresentButton:(id)sender
{
    JVAlertViewController * controller = [[JVAlertViewController alloc] initWithAlertMessage:@"Pour vous garantir la meilleure expérience possible, jerorsenville aimerait accéder à votre géolocalisation"];
    
    controller.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    
    controller.dismissByBackgoundTap = YES;

    [controller actionAcceptWithTitle:@"OUI" actionHandler:^{
        NSLog(@"accetable");
    }];
    [controller actionDeclineWithTitle:@"NON" actionHandler:^{
        NSLog(@"decline buton");
    }];
    [self presentViewController:controller animated:YES completion:nil];
}


@end
